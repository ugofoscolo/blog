# This file is a template, and might need editing before it works on your project.
FROM httpd:alpine

RUN sed -i 's/80/5000/g' /usr/local/apache2/conf/httpd.conf

EXPOSE 5000

COPY _site/ /usr/local/apache2/htdocs/
