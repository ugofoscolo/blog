---
layout: page
title: Wishlist
---

A quanto pare sono una persona piuttosto complicata alla quale fare regali, per questo motivo dopo svariate richieste ho deciso di creare (e mi impegno a mantenere nel tempo) una wishlist per aiutare il malcapitato di turno. Per eventuali dubbi o altro rompete pure le scatole alla mia dolce metá.

*Sempre verdi*

  * <i class="fas fa-utensils"></i> Accessori cucina
  * <i class="fas fa-guitar"></i> Accessori chitarra (suggerimenti da [Gastube](https://www.youtube.com/watch?v=uJ0I_wvOas0))
  * <i class="fas fa-wine-bottle"></i> Whiskey (torbati tipo scozzese o giapponesi), rhum agricoli, birra artigianale (luppolate in genere, ipa, porter e stout, meglio se imperial)
  * <i class="fas fa-user-tie"></i> Buono per taglio/rasatura [Blues Barber Shop](https://bluesbarber.it)
  * <i class="fas fa-user-tie"></i> Camicie che non si stirano (sobrie)

*Under 50€*

  * <i class="fas fa-guitar"></i> [Plettri chitarra 1](https://www.amazon.it/Jim-Dunlop-482P-Plettro-confezione/dp/B00JFGPYWY/) <i class="fas fa-boxes"></i> <i class="fab fa-amazon"></i>
  * <i class="fas fa-guitar"></i> [Plettri chitarra 2](https://www.amazon.it/Jim-Dunlop-427PJP-Plettri-Spessore/dp/B0117TLSZS) <i class="fas fa-boxes"></i> <i class="fab fa-amazon"></i>
  * <i class="fas fa-user-tie"></i> [Acqua di colonia Proraso Azure Lime](https://www.amazon.it/dp/B06XDMYRTR) <i class="fas fa-boxes"></i> <i class="fab fa-amazon"></i>
  * <i class="fas fa-guitar"></i> [Kit Manutenzione Chitarra](https://www.thomann.de/it/ernie_ball_tool_kit_451436.htm?sid=5d111a9b5bc930cd7c96083a03efd2a9) <i class="fas fa-box"></i>

*50-100€*

  * <i class="fas fa-microchip"></i> [NanoLeaf panels](https://nanoleaf.me) <i class="fas fa-box"></i>

*Over 100€*

  * <i class="fas fa-user-tie"></i> [Francesine Velasca in pelle di vitello liscia (tg. 45)](https://it.velasca.com/pages/francesine) <i class="fas fa-boxes"></i>
  * <i class="fas fa-guitar"></i> [Nabla The Blue](https://www.nablacustom.com/it/overdrive/3-the-blue.html) <i class="fas fa-box"></i>
  * <i class="fas fa-guitar"></i> [Spark Amp + Bag](https://www.positivegrid.com/spark/) <i class="fas fa-box"></i>
  * <i class="fas fa-motorcycle"></i> [Navigatore Garmin Zumo XT](https://www.motoabbigliamento.it/navigatore-garmin-zumo-xt) <i class="fas fa-box"></i>

*Legenda*

<i class="fas fa-box"></i> - Regalo una tantum<br />
<i class="fas fa-boxes"></i> - Regalo ripetibile<br />
<i class="fab fa-amazon"></i> - Disponibile su Amazon<br />
<br />
<i class="fas fa-utensils"></i> - Cucina<br />
<i class="fas fa-wine-bottle"></i> - Drinks<br />
<i class="fas fa-user-tie"></i> - Fashon<br />
<i class="fas fa-motorcycle"></i> - Moto<br />
<i class="fas fa-guitar"></i> - Musica<br />
<i class="fas fa-microchip"></i> - Tecnologia
