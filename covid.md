---
layout: page
title: COVID-19
---

In questa pagina raccolgo i [dati ufficiali](https://github.com/pcm-dpc/COVID-19/blob/master/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv) sulla diffusione del virus in Italia e creo qualche grafico per poterli analizzare pi&uacute; comodamente.

## Ultimi Dati

{% assign covidata = site.data.covid %}
{% assign prelast = site.data.last.index | minus: 2 %}
{% assign dguariti = covidata.last.dimessi_guariti | minus: covidata[prelast].dimessi_guariti %}
{% assign ddeceduti = covidata.last.deceduti | minus: covidata[prelast].deceduti %}
{% assign pguariti = 100.0 | times: covidata.last.dimessi_guariti | divided_by: covidata.last.totale_casi | precision: 2 %}
{% assign dtotalecasi = covidata.last.totale_casi | minus: covidata[prelast].totale_casi %}
{% assign pdeceduti = 100.0 | times: covidata.last.deceduti | divided_by: covidata.last.totale_casi | precision: 2 %}
{% assign pisodom = 100.0 | times: covidata.last.isolamento_domiciliare | divided_by: covidata.last.totale_positivi | precision: 2 %}
{% assign pospedale = 100.0 | times: covidata.last.totale_ospedalizzati | divided_by: covidata.last.totale_positivi | precision: 2 %}
{% assign pterapia = 100.0 | times: covidata.last.terapia_intensiva | divided_by: covidata.last.totale_positivi | precision: 2 %}
<table id="covid-resume" class="covid-table-nojs">
<thead>
<tr><th>∆ Casi Attivi</th>
<th>∆ Totale Casi</th>
<th>Nuove Guarigioni</th>
<th>Nuovi Decessi</th></tr>
</thead>
<tbody>
<tr><td>{{ covidata.last.variazione_totale_positivi | intcomma: ',' }}</td>
<td>{{ covidata.last.nuovi_positivi | intcomma: ',' }}</td>
<td>{{ dguariti | intcomma: ',' }}</td>
<td>{{ ddeceduti | intcomma: ',' }}</td></tr>
</tbody>
<tfoot>
<tr><td colspan="4" align="right"><i>Giorno di riferimento: {{ covidata.last.data | date: "%Y-%m-%d" }}</i></td></tr>
</tfoot>
</table>

## Percentuali Generali

<table id="covid-percentage" class="covid-table-nojs">
<thead>
<tr><th>Percentuale Guariti</th>
<th>Percentuale Deceduti</th>
<th>Percentuale Isolamento Domiciliare \*</th>
<th>Percentuale Ricoverati \*</th>
<th>Percentuale Terapia Intensiva \*</th></tr>
</thead>
<tbody>
<tr><td>{{ pguariti }}%</td>
<td>{{ pdeceduti }}%</td>
<td>{{ pisodom }}%</td>
<td>{{ pospedale }}%</td>
<td>{{ pterapia | precision: 2 }}%</td></tr>
</tbody>
<tfoot>
<tr><td colspan="5" align="right"><i>* Riferita ai Casi Attuali</i></td></tr>
</tfoot>
</table>

## Nuovi Casi

[![dati-nuovi-casi](/assets/img/covid-nuovi-casi.png)](/assets/img/covid-nuovi-casi.png)
[![dati-nuovi-casi-log](/assets/img/covid-nuovi-casi-log.png)](/assets/img/covid-nuovi-casi-log.png)

<table id="covid-nuovi-casi" class="covid-table">
<thead>
<tr><th>Data</th>
<th>Nuovi Casi</th>
<th>Percentuale Nuovi Casi</th>
<th>Casi Attualmente Attivi</th></tr>
</thead>
<tbody>
{% for cday in site.data.covid reversed %}
<tr><td>{{ cday.data | date: "%Y-%m-%d" }}</td>
<td>{{cday.nuovi_positivi | intcomma: ','}}</td>
<td>{{ 100.0 | divided_by: cday.totale_positivi | times: cday.nuovi_positivi | precision: 2 | split: ","}}%</td>
<td>{{cday.totale_positivi | intcomma: ','}}</td></tr>
{% endfor %}
</tbody>
</table>

## Tamponi Effettuati e Totale Casi

[![dati-tamponi](/assets/img/covid-tamponi.png)](/assets/img/covid-tamponi.png)
[![dati-regioni-tamponi](/assets/img/covid-regioni-situazione-tamponi.png)](/assets/img/covid-regioni-situazione-tamponi.png)

<table id="covid-tamponi" class="covid-table">
<thead>
<tr><th>Data</th>
<th>Totale Casi</th>
<th>Percentuale Casi</th>
<th>Tamponi</th></tr>
</thead>
<tbody>
{% for cday in site.data.covid reversed %}
<tr><td>{{ cday.data | date: "%Y-%m-%d" }}</td>
<td>{{cday.totale_casi | intcomma: ',' }}</td>
<td>{{ 100.0 | divided_by: cday.tamponi | times: cday.totale_casi | precision: 2 }}%</td>
<td>{{cday.tamponi | intcomma: ','}}</td></tr>
{% endfor %}
</tbody>
</table>

## Guariti e Deceduti rispetto al Totale Casi

[![dati-guariti-decessi](/assets/img/covid-guariti-decessi.png)](/assets/img/covid-guariti-decessi.png)
[![dati-guariti-decessi-perc](/assets/img/covid-guariti-decessi-perc.png)](/assets/img/covid-guariti-decessi-perc.png)
[![dati-guariti-decessi](/assets/img/covid-regioni-guariti-decessi.png)](/assets/img/covid-regioni-guariti-decessi.png)

<table id="covid-guariti-decessi" class="covid-table">
<thead>
<tr><th>Data</th>
<th>Dimessi Guariti</th>
<th>Percentuale Guariti</th>
<th>Deceduti</th>
<th>Percentuale Deceduti</th>
<th>Totale Casi</th></tr>
</thead>
<tbody>
{% for cday in site.data.covid reversed %}
<tr><td>{{ cday.data | date: "%Y-%m-%d" }}</td>
<td>{{cday.dimessi_guariti | intcomma: ','}}</td>
<td>{{ 100.0 | times: cday.dimessi_guariti | divided_by: cday.totale_casi | precision: 2 }}%</td>
<td>{{cday.deceduti | intcomma: ','}}</td>
<td>{{ 100.0 | times: cday.deceduti | divided_by: cday.totale_casi | precision: 2 }}%</td>
<td>{{cday.totale_casi | intcomma: ','}}</td></tr>
{% endfor %}
</tbody>
</table>

## Situazione Ricoveri

[![dati-situazione-ricoveri](/assets/img/covid-situazione-ricoveri.png)](/assets/img/covid-situazione-ricoveri.png)
[![dati-situazione-ricoveri](/assets/img/covid-situazione-ricoveri-delta.png)](/assets/img/covid-situazione-ricoveri-delta.png)
[![dati-regioni-situazione-ricoveri](/assets/img/covid-regioni-situazione-ricoveri.png)](/assets/img/covid-regioni-situazione-ricoveri.png)

<table id="covid-situazione-ricoveri" class="covid-table">
<thead>
<tr><th>Data</th>
<th>Ricoverati con Sintomi</th>
<th>Terapia Intensiva</th>
<th>Isolamento Domiciliare</th>
<th>Casi Attualmente Positivi</th></tr>
</thead>
<tbody>
{% for cday in site.data.covid reversed %}
<tr><td>{{ cday.data | date: "%Y-%m-%d" }}</td>
<td>{{cday.ricoverati_con_sintomi | intcomma: ','}}</td>
<td>{{cday.terapia_intensiva | intcomma: ','}}</td>
<td>{{cday.isolamento_domiciliare | intcomma: ','}}</td>
<td>{{cday.totale_positivi | intcomma: ','}}</td></tr>
{% endfor %}
</tbody>
</table>

## Dettaglio Situazione Ricoveri per Regione

{% assign i = 1 %}
{% assign regdata_hash = site.data.regdata %}
{% for region in regdata_hash %}
{% if i < 10 %}
{% assign file = "/assets/img/regdata/covid-regioni-situazione-ricoveri-r0" | append: i | append: ".png" %}
{% else %}
{% assign file = "/assets/img/regdata/covid-regioni-situazione-ricoveri-r" | append: i | append: ".png" %}
{% endif %}
{% if i != 4 and i != 23 %}
[![regimage]({{file}})]({{file}})
{% for regdata in region %}
{% comment %}
<table="regdata-{{i}}" class="covid-table">
<thead>
<tr><th>Data</th>
<th>Ricoverati con Sintomi</th>
<th>Terapia Intensiva</th>
<th>Isolamento Domiciliare</th>
<th>Casi Attualmente Positivi</th></tr>
</thead>
<tbody>
{% for row in regdata %}
<tr><td>{{row.data}}</td>
<td>{{row.isolamento_domiciliare}}</td>
<td>{{row.ricoverati_con_sintomi}}</td>
<td>{{row.terapia_intensiva}}</td>
<td>{{row.totale_positivi}}</td></tr>
{% endfor %}
</tbody></table>
{% endcomment %}
{% endfor %}
{% endif %}
{% assign i = i | plus: 1 %}
{% endfor %}

## Note

<table id="covid-note" class="covid-table">
<thead>
<tr><th>Data</th>
<th>Nota</th></tr>
</thead>
<tbody>
{% for nota in site.data.note reversed %}
<tr><td>{{ nota.data | date: "%Y-%m-%d"}}</td>
<td>{{ nota.note }}</td></tr>
{% endfor %}
</tbody>
</table>
