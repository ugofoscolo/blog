---
layout: page
title: Ambiente
comments: false
---
In questa pagina elenco alcuni progetti interessanti che ho trovato a tutela dell'ambiente e che ho deciso di sostenere.

# 3Bee
_[https://www.3bee.it/](https://www.3bee.it/)_  
&Eacute; una azienda agri-tech che sviluppa sistemi intelligenti di monitoraggio e diagnostica per gli animali. Il suo primo focus sono state le api e la biodiversità. Unendo elettronica e biologia hanno sviluppato un sistema di monitoraggio dello stato di salute degli alveari. Il sistema permette di analizzare i bisogni e le necessità delle api così da intervenire in modo mirato in caso di problemi.
Dal loro sito &eacute; anche possibile adottare un'arnia a distanza.


# Treedom
_[https://www.treedom.net/](https://www.treedom.net/)_  
Treedom &eacute; l’unica piattaforma web al mondo che permette di piantare un albero a distanza e seguirlo online.
Dalla sua fondazione, avvenuta nel 2010 a Firenze, sono stati piantati più di 1.000.000 di alberi in Africa, America Latina, Asia e Italia. Tutti gli alberi vengono piantati direttamente da contadini locali e contribuiscono a produrre benefici ambientali, sociali ed economici. Grazie a tale business model, Treedom fa parte dal 2014 delle Certified B Corporations, il network di imprese che si contraddistinguono per elevate performance ambientali e sociali.
Ogni albero di Treedom ha una pagina online, viene geolocalizzato e fotografato, può essere custodito o regalato virtualmente a terzi. Grazie a queste caratteristiche, l’albero di Treedom coinvolge le persone ed &eacute; al tempo stesso uno strumento di comunicazione e marketing per aziende.
