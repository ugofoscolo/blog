#!/bin/bash

cd blog

git pull origin master

echo "Fetching covid last data"
wget https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv -O _data/covid.csv
wget https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-regioni/dpc-covid19-ita-regioni-latest.csv -O _data/covid-reg.csv
wget https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-regioni/dpc-covid19-ita-regioni.csv -O _data/covid-regh.csv
wget https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/note/dpc-covid19-ita-note.csv -O _data/note.csv

echo "Removing last blank line"
sed -i '${/^$/d;}' _data/covid.csv
sed -i '${/^$/d;}' _data/covid-reg.csv
sed -i '${/^$/d;}' _data/covid-regh.csv

echo "Removing mistakes"
sed -i -e '/^[a-z].*\"$/d'  _data/note.csv

echo "Splitting regional data"
for i in `seq -w 1 22`; do
	head -n1 _data/covid-regh.csv > _data/regdata/reg-$i.csv
done

rm -fv _data/regdata/reg-codice_regione.csv
rm -fv _data/regdata/reg-.csv

awk -F\, '{ print $0 >> ("_data/regdata/reg-"$3".csv")}{close ("_data//regdata/reg-"$3".csv")}' _data/covid-regh.csv

find _gnuplot -name '*gnuplot' -exec gnuplot -c {} \;

git add _data
git add assets/img
git add assets/img/regdata

git commit -m"Adding covid data - `date`"
git push origin master
