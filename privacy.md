---
layout: page
title: Privacy Policy
---
Questo sito utilizza i cookie (https://it.wikipedia.org/wiki/Cookie). In questo sito, i cookie vengono utilizzati per affinit&agrave; di sessione, memorizzare le preferenze dell'utente e fornire dati di tracciamento **anonimi** ad applicazioni come Google Analytics. Sottolino l'anonimato, poiché non vengono raccolte informazioni di identificazione personale sul visitatore in alcun modo.  
Esempi di dati di tracciamento anonimi raccolti su questo sito web:  

* La visualizzazione della pagina che visiti e come sei arrivato / navigato a quella pagina.
* Allo stesso modo, i pulsanti di condivisione social, possono inviare i dati al mio account Google Analytics e potenzialmente a un monitoraggio di terze parti - ad esempio LinkedIn, Twitter, Facebook ecc.

Il banner visualizzato alla prima visita ti consente di acconsentire alla raccolta delle tue informazioni anonime su questo sito web. Se non si desidera acconsentire, il banner deve rimanere in mostra.  
Di norma, i cookie migliorano la tua esperienza di navigazione. Tuttavia, potresti preferire disabilitare i cookie su questo sito e su altri. Il modo più efficace per farlo è disabilitare i cookie nel tuo browser. Ti suggerisco di consultare la sezione Aiuto del tuo browser.  
Per ulteriori informazioni su come Google Analytics utilizza i cookie e i dati dell'indirizzo IP, vedere: https://support.google.com/analytics/answer/2838718?hl=it.
