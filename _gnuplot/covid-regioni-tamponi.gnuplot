set title 'Situazione Regionale Tamponi'
set term pngcairo size 1200,800
set termoption dashed
set key outside center bottom horizontal spacing 3
set xlabel 'Regioni'
set ylabel 'Numero Casi (in migliaia - log)'
set grid ls 100
set style data histograms
set style fill solid 1.0 border -1
set ytics nomirror
set datafile separator ','
set autoscale x
set logscale y 2
set output 'assets/img/covid-regioni-situazione-tamponi.png'
set xtics rotate by 45 right
plot '_data/covid-reg.csv' skip 1 using (column(18)/1000):xtic(4) title 'Totale Casi' linecolor rgb 'red', \
  '' skip 1 using (column(19)/1000):xtic(4) title 'Tamponi' linecolor rgb 'blue'
# data,stato,codice_regione,denominazione_regione,lat,long,ricoverati_con_sintomi,terapia_intensiva,totale_ospedalizzati,isolamento_domiciliare,totale_attualmente_positivi,nuovi_attualmente_positivi,dimessi_guariti,deceduti,totale_casi,tamponi
