set title 'Tamponi effettuati e Totale Casi'
set term pngcairo size 1200,800
set termoption dashed
set key outside center bottom horizontal spacing 3
set xlabel 'Data'
set ylabel 'Numero (in milioni - log)'
set y2label '% Casi'
set y2tics
set grid ls 100
set style line 100 lt 1 lc rgb 'grey' lw 0.2
set datafile separator ','
set xdata time
set format x '%d/%m'
set logscale y 2
set style line 101 lw 2 lt rgb 'blue'
set style line 102 lw 2 lt rgb '#228b22' #forestgreen
set style line 103 lw 2 lt rgb 'red'
set style line 1 lw 2 lt rgb 'red' dt 2
set timefmt '%Y-%m-%d %H:%M:%S'
set output 'assets/img/covid-tamponi.png'
set xrange ['2020-02-24':]
set yrange [0:]
set y2range [0:]
plot '_data/covid.csv' skip 1 using 1:(column(14)/1000000) t 'Totale Casi' w lines ls 103, \
  '' using 1:(column(15)/1000000) t 'Tamponi Effettuati' w lines ls 101, \
  '' using 1:(100/column(15)*column(14)) t '% Casi' w lines ls 1 axis x1y2
