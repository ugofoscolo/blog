set title 'Guariti e Deceduti rispetto al Totale Casi'
set term pngcairo size 1200,800
set termoption dashed
set key outside center bottom horizontal spacing 3
set xlabel 'Data'
set ylabel 'Totale Casi'
set y2label 'Percentuale Guariti e Deceduti'
set y2tics
set grid ls 100
set style line 100 lt 1 lc rgb 'grey' lw 0.2
set ytics nomirror
set datafile separator ','
set xdata time
set format x '%d/%m'
set autoscale y
set autoscale y2
set style line 101 lw 2 lt rgb 'black' dt 2
set style line 102 lw 2 lt rgb '#228b22' #forestgreen
set style line 103 lw 2 lt rgb 'red'
set timefmt '%Y-%m-%d %H:%M:%S'
set output 'assets/img/covid-guariti-decessi-perc.png'
set xrange ['2020-02-24':]
set yrange [0:]
plot '_data/covid.csv' skip 1 using 1:14 t 'Totale Casi' w lines ls 101, \
  '' using 1:(100.0/column(14)*column(10)) t '% Guariti' w lines axis x1y2 ls 102, \
  '' using 1:(100.0/column(14)*column(11)) t '% Deceduti' w lines axis x1y2 ls 103
