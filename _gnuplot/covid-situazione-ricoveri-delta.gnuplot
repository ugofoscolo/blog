set title 'Situazione Ricoveri ∆'
set term pngcairo size 1200,800
set termoption dashed
set key outside center bottom horizontal spacing 3
set xlabel 'Data'
set ylabel 'Totale Casi'
set grid ls 100
set style line 100 lt 1 lc rgb 'grey' lw 0.2
set datafile separator ','
set xdata time
set format x '%d/%m'
set style line 101 lw 2 lt rgb 'blue'
set style line 102 lw 2 lt rgb '#228b22' #forestgreen
set style line 103 lw 2 lt rgb 'red'
set style line 104 lw 2 lt rgb 'blue' dt 2
set style line 105 lw 2 lt rgb 'black' dt 2
set timefmt '%Y-%m-%d %H:%M:%S'
set output 'assets/img/covid-situazione-ricoveri-delta.png'
set xrange ['2020-02-24':]
y_olds = y_oldi = y_oldd = y_oldp = 0
plot '_data/covid.csv' skip 1 using 1:(temps=column(3)-y_olds , y_olds=column(3) , temps) t 'Ricoverati con Sintomi' w lines ls 101, \
  '' using 1:(tempi=column(4)-y_oldi , y_oldi=column(4) , tempi) t 'Terapia Intensiva' w lines ls 103, \
  '' using 1:(tempd=column(6)-y_oldd , y_oldd=column(6) , tempd) t 'Isolamento domiciliare' w lines ls 102, \
  '' using 1:(column(8)) t 'Casi Attualmente Positivi' w lines ls 104, \
  '' using 1:(column(9)) t 'Casi Totali' w lines ls 105
