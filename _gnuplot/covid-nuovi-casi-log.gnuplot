set title 'Andamento Logaritmico Nuovi Casi
set term pngcairo size 1200,800
set termoption dashed
set key outside center bottom horizontal spacing 3
set ylabel 'Nuovi Casi
set xlabel 'Totale Casi Attivi'
set grid ls 100
set style line 100 lt 1 lc rgb 'grey' lw 0.2
set datafile separator ','
set logscale xy 10
set autoscale x
set yrange [70:]
set style fill transparent solid 0.7
set style line 101 lw 2 lt rgb 'blue'
set style line 102 lw 2 lt rgb '#228b22' #forestgreen
set style line 103 lw 2 lt rgb 'red'
set style line 1 lw 2 lt rgb 'red' dt 2
set output 'assets/img/covid-nuovi-casi-log.png'
plot '_data/covid.csv' skip 1 using 7:9 t 'Andamento Logaritmico Nuovi Casi' w lines ls 101
