set title 'Situazione Ricoveri Molise'
set output 'assets/img/regdata/covid-regioni-situazione-ricoveri-r14.png'
set term pngcairo size 1200,800
set termoption dashed
set key outside center bottom horizontal spacing 3
set xlabel 'Data'
set ylabel 'Totale Casi'
set grid ls 100
set style line 100 lt 1 lc rgb 'grey' lw 0.2
set datafile separator ','
set xdata time
set format x '%d/%m'
set logscale y 2
set style line 101 lw 2 lt rgb 'blue'
set style line 102 lw 2 lt rgb '#228b22' #forestgreen
set style line 103 lw 2 lt rgb 'red'
set style line 104 lw 2 lt rgb 'blue' dt 2
set timefmt '%Y-%m-%d %H:%M:%S'
set xrange ['2020-02-24':]
plot '_data/regdata/reg-14.csv' skip 1 using 1:7 t 'Ricoverati con Sintomi' w lines ls 101, \
  '' using 1:8 t 'Terapia Intensiva' w lines ls 103, \
  '' using 1:10 t 'Isolamento domiciliare' w lines ls 102, \
  '' using 1:11 t 'Casi Attualmente Positivi' w lines ls 104
