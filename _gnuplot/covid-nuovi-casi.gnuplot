set title 'Nuovi Casi rispetto agli Attualmente Positivi'
set term pngcairo size 1200,800
set termoption dashed
set key outside center bottom horizontal spacing 3
set xlabel 'Data'
set ylabel 'Numero Casi (log)'
set y2label '% Nuovi Casi'
set y2tics
set grid ls 100
set style line 100 lt 1 lc rgb 'grey' lw 0.2
set ytics nomirror
set datafile separator ','
set xdata time
set format x '%d/%m'
set logscale y 2
set y2range [0:60]
set style fill transparent solid 0.7
set style line 101 lw 2 lt rgb 'blue'
set style line 102 lw 2 lt rgb '#228b22' #forestgreen
set style line 103 lw 2 lt rgb 'red'
set style line 1 lw 2 lt rgb 'red' dt 2
set timefmt '%Y-%m-%d %H:%M:%S'
set output 'assets/img/covid-nuovi-casi.png'
set xrange ['2020-02-24':]
plot '_data/covid.csv' skip 1 using 1:7 t 'Casi Attualmente Positivi' w lines ls 101, \
  '' using 1:9 t 'Nuovi Casi' w lines ls 103, \
  '' using 1:(100/column(7)*column(9)) t '% Nuovi Casi' w lines axis x1y2 ls 1
