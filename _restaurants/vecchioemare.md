---
title: Il Vecchio ed il Mare
layout: restaurant
web: https://facebook.com/IlVecchioeilMareFirenze
address: Via Vincenzo Gioberti, 61N
cap: 50121
city: Firenze
tel: +39055669575
special: Pizza alla pala e Pesce
location:
  latitude: 43.7699799
  longitude: 11.2739541
  url: ""
---
