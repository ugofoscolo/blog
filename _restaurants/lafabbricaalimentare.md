---
title: La Fabbrica Alimentare
layout: restaurant
web: https://www.facebook.com/fabbricaalimentare/
address: Via Valentini 102
cap: 59100
city: Prato (PO)
tel: +390574870315
special: Ramen Bar
location:
  latitude: 43.8682283
  longitude: 11.0947618
  url: ""
---
