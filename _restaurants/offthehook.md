---
title: Off The Hook
layout: restaurant
web: https://www.thefork.it/ristorante/off-the-hook-r71268
address: Via Giuseppe Verdi 47
cap: 50122
city: Firenze (FI)
tel: +3905519991333
special: Hamburger e Birra
location:
  latitude: 43.770785
  longitude: 11.2597825
  url: ""
---
