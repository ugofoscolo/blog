---
title: Lo Spigo
layout: restaurant
address: Via di Pulica 99
cap: 50056
city: Montelupo
tel: +393317387762
special: Cucina Toscana, Pizza e Pesce
location:
  latitude: 43.6975097
  longitude: 11.0493202
  url: ""
---
