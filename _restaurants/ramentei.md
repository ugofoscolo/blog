---
title: Ramen Tei
layout: restaurant
web: http://www.ramentei.it
address: Viale Spartaco Lavagnini, 38a
cap: 50129
city: Firenze
tel: +390554648528
special: Ramen
location:
  latitude: 43.7828908
  longitude: 11.2540437
  url: ""
---
