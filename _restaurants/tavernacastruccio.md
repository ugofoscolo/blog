---
title: La Taverna di Castruccio
layout: restaurant
web: https://www.latavernadicastruccio.com
address: Via Pisana, 129
cap: 50018
city: Scandicci (FI)
tel: +39055751198
special: Cucina Tradizionale
location:
  latitude: 43.7642555
  longitude: 11.1777544
  url: ""
---
