---
title: ZeroZero
layout: restaurant
web: 
address: Via Giovanni Lorenzoni, 8/r
cap: 50134
city: Firenze
tel: +39055495000
special: Pizza alla pala
location:
  latitude: 43.7879853
  longitude: 11.2424709
  url: ""
---
