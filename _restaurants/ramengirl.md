---
title: Ramen Girl
layout: restaurant
web: https://ramengirlbyenotecalucafirenze.blogspot.com
address: Via Francesco Talenti, 144
cap: 50142
city: Firenze
tel: +390555120437
special: Ramen
location:
  latitude: 43.7735409
  longitude: 11.21182
  url: ""
---
