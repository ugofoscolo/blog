---
title: Duje
layout: restaurant
web: https://duje.it
address: Largo Pietro Annigoni, 9/c
cap: 50122
city: Firenze
tel: +39055245829
special: Pizza
location:
  latitude: 43.77061
  longitude: 11.2665473
  url: ""
---
Duje è un concept nato nel 2015 da un’intuizione prima di Pietro Baracco, condivisa con Simone Fiesoli. Entrambi imprenditori toscani, avevano un sogno che poi è diventato realtà: valorizzare la qualità della pizza d’ispirazione napoletana, il prodotto più riconoscibile nel mondo e rappresentativo del made in Italy. E nel segno della qualità Pietro e Simone portano avanti un progetto che ha l’ambizione di crescere sempre più.  
Da qui la scelta di creare una location accogliente e curata in ogni particolare, nonché di chiamare un maestro pizzaiolo come Michele Leo mettendo il suo patrimonio di conoscenza a disposizione dei clienti. Lui e gli altri artigiani della pizza ogni giorno sono al lavoro per raggiungere il comune obbiettivo: portare sulle tavole la passione e l’arte del pizzaiolo napoletano (riconosciuto dall’Unesco come Patrimonio Immateriale dell’ Umanità).
