---
title: Acqua Matta
layout: restaurant
web: https://www.acquamatta.eu/
address: Via Barberinese 157
cap: 50013
city: Campi Bisenzio (FI)
tel: +390558954319
special: Pizza e Pesce
location:
  latitude: 43.8176095
  longitude: 11.1198729
  url: "https://g.page/acquamatta-campi-bisenzio?share"
---
