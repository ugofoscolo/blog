---
title: FishingLab
layout: restaurant
web: https://www.fishinglab.it
address: Via del Proconsolo, 16r
cap: 50122
city: Firenze
tel: +39055240618
special: Pesce
location:
  latitude: 43.7709689
  longitude: 11.255752
  url: ""
---
