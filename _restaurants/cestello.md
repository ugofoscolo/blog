---
title: Cestello Ristoclub
layout: restaurant
web: http://www.cestelloristoclub.com/
address: Piazza del Cestello 8
cap: 50124
city: Firenze
tel: +390552645364
special: Pesce (Ristorante di lusso)
location:
  latitude: 43.770298
  longitude: 11.2413539
  url: "https://goo.gl/maps/yJSjmGF3UgJ7Q6GG6"
---
