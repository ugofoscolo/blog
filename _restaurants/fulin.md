---
title: Fulin
layout: restaurant
web: http://www.fulin.it
address: Via Giampaolo Orsini 113r
cap: 50125
city: Firenze (FI)
tel: +39055684931
special: Luxury Chinese Experience
location:
  latitude: 43.763179
  longitude: 11.273835
  url: ""
---
