---
layout: page
title: La Guida ai Ristoranti di Ugo
---
Questa pagina è la mia personale lista di ristoranti preferiti, ha come unico scopo quello di farmeli vedere comodamente su una mappa ed avere a portata numeri di telefono e affini.  
Non è la guida Michelin, non ha scopo commerciale, non è sponsorizzata nè sponsorizzabile, è esclusivamente a mio uso e consumo.  
{% google_map width="100%" src="_restaurants" %}

{% for restaurant in site.restaurants %}
## {{ restaurant.title }}
{% if restaurant.web %}Web: [{{ restaurant.web }}]({{ restaurant.web }})  {% endif %}
{% if restaurant.address %}Indirizzo: {{ restaurant.address }} - {{ restaurant.cap }} - {{ restaurant.city }}  {% endif %}
Tel: <a href="tel:+{{ restaurant.tel }}">+{{ restaurant.tel }}</a>
Specialità: {{ restaurant.special }}
{% endfor %}
