// All others
$(document).ready(function() {
  $('.covid-table').bind('dynatable:preinit').dynatable({
    features: {
      paginate: true,
      search: false,
      recordCount: false,
      perPageSelect: true
    }
  });
});
